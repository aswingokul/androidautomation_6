/**
 * 
 */
package pageObjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import player.AbstractPlayer;

/**
 * @author aswin
 *
 */
public class RadioPlayer extends AbstractPlayer{
	
	MobileElement backToQueue = null;
	MobileElement likeButton = null;
	MobileElement dislikeButton = null;
	MobileElement artistDiscoveryToggle = null;
	
	
	public RadioPlayer(AndroidDriver<MobileElement> driver) {
		
//		System.out.println("Inside Radio Player Constructor");
		
		driver.findElementById("player_title").click();
		super.initialize(driver);
//		System.out.println("Initializing Radio Player specific controls.....");
		artistDiscoveryToggle = driver.findElementById("com.saavn.android:id/modeSwitch");
		backToQueue = driver.findElementById("com.saavn.android:id/backToQueueBtn");
		likeButton = driver.findElementById("com.saavn.android:id/radiolikebutton");
		dislikeButton = driver.findElementById("com.saavn.android:id/radiodislikebutton");
		
		
//		System.out.println("Initialized all the Radio PLayer controls");
	}
	
	public void backToQ() {
		backToQueue.click();
	}
	
	public void likeSong() {
		likeButton.click();
	}
	
	public void dislikeSong() {
		dislikeButton.click();
	}
	
	public void switchMode() {
		artistDiscoveryToggle.click();
	}
	
	

}
