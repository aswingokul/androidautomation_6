/**
 * 
 */
package pageObjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import player.AbstractPlayer;

/**
 * @author aswin
 *
 */
public class QueuePlayer extends AbstractPlayer{
	
	private MobileElement shuffleButton = null;
	private MobileElement loopButton = null;
	private MobileElement playPrevButton = null;
	
	
	@Override
	protected void initialize(AndroidDriver<MobileElement> driver) {
		driver.findElementById("player_title").click();
		super.initialize(driver);
		
		shuffleButton = driver.findElementById("com.saavn.android:id/shuffleButton");
		loopButton = driver.findElementById("com.saavn.android:id/loopButton");
		playPrevButton = driver.findElementById("com.saavn.android:id/previousButton");
		
	}
	
	public QueuePlayer(AndroidDriver<MobileElement> driver) {
		System.out.println("Inside the QueuePlayer constructor");
		initialize(driver);
	}
	
	public void shuffle() {
		shuffleButton.click();
	}
	
	public void loop() {
		loopButton.click();
	}
	
	public void previous() {
		playPrevButton.click();
	}

}
