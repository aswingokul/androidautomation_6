/**
 * 
 */
package player;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

/**
 * @author aswin
 *
 */
public abstract class AbstractPlayer {
	
	protected MobileElement albumArt = null;
	protected MobileElement playPauseButton = null;
	protected MobileElement playNextButton = null;
	protected MobileElement seekbar = null;
	protected MobileElement currSong = null;
	
	public void playNext() {
		playNextButton.click();
	}
	
	public void playPause() {
		playPauseButton.click();
	}
	
	public String getCurrentSong() {
		String songName = currSong.getText();
		System.out.println("Currently playing Song: " + songName);
		return songName;
	}
	
	protected void initialize(AndroidDriver<MobileElement> driver) {
		if(driver != null) {
//			System.out.println("Inside the Abstract Player Constructor");
			playPauseButton = driver.findElementById("com.saavn.android:id/radioplaybutton");
			playNextButton  = driver.findElementById("com.saavn.android:id/radionext");
			currSong = driver.findElementById("com.saavn.android:id/curr_song_title");
//			System.out.println("Initialized all the Abstract Player controls...");
		}else {
			System.err.println("AbstractPlayer: initialize - Driver is null");
		}
	}

}
