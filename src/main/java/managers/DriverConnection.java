/**
 * 
 */
package managers;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import enums.DriverType;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

/**
 * @author aswin
 *
 */
public class DriverConnection {
	private static AndroidDriver<MobileElement> driver = null;
	
	/*
	 * The constructor actually creates the instance of the driver, which is tailored according to the capabilities
	 * 
	 * Follows a Singleton design pattern
	 */
	public DriverConnection(DriverType driverType, DesiredCapabilities capabilities){
		if(driverType == DriverType.DEVICE){
			try {
				System.out.println("In DriverConnection constructor");
				 driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:3678/wd/hub"), capabilities);
				 if(driver == null){
					 System.out.println("Driver is still null....");
				 }else{
					 System.out.println("ESHWARA!");
				 }
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	/*
	 * Public method to get the driver instance
	 */
	public static AndroidDriver<MobileElement> getDriver(DriverType driverType, DesiredCapabilities capabilities){
		System.out.println("DriverConnection - getDriver()");
		if(driver == null){
			new DriverConnection(driverType, capabilities);
			
		}
		System.out.println("Driver in DriverConnection - " + driver.toString());
		return driver;
	}
	
	/*
	 * Public method to get the driver instance
	 */
	public static AndroidDriver<MobileElement> getDriver(){
		return driver;
	}
}
