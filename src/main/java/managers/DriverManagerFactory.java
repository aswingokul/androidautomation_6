/**
 * 
 */
package managers;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import capabilities.DeviceCapabilities;
import enums.DriverType;
/**
 * @author aswin
 *
 */
public class DriverManagerFactory {
	/*
	 * A factory method to call the appropriate implementation based on the Driver type
	 * 
	 * DriverType - what kind of driver you want, which depends on the device
	 * Devices could be cloud, say, SauceLabs or any Android Device
	 */
	public static DriverManager getManager(DriverType type){
		DriverManager driverManager = null;
		if(type == DriverType.DEVICE){
			driverManager = new DeviceCapabilities();
		}
		return driverManager;
	}
}
