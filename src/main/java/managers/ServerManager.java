/**
 * 
 */
package managers;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import io.appium.java_client.service.local.AppiumServiceBuilder;
import properties.MyLogger;

/**
 * @author aswin
 *
 */
public class ServerManager {
	/*
	 * This function programmatically starts the Appium server using the node.js script 
	 */
	
	public static void startAppiumServer(){
		MyLogger.log.info("------Starting Appium Server--------");
		AppiumServiceBuilder service = new AppiumServiceBuilder().usingDriverExecutable(new File("/usr/local/bin/node"))
				.withAppiumJS(new File("/Applications/Appium.app/Contents/Resources/app/node_modules/appium/build/lib/main.js"));
		service.build().start();
		MyLogger.log.info("------Appium Server Started--------");		
	}
	
	/*
	 * For stopping the server. Both the functions are called once for suite
	 */
	
	public static void stopAppiumServer(){
		String[] commands = {"/usr/bin/killall", "-KILL", "node"};
		try {
			Runtime.getRuntime().exec(commands);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
