/**
 * 
 */
package managers;



import capabilities.DeviceCapabilities;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import properties.MyLogger;

/**
 * @author aswin
 *
 */
public abstract class DriverManager {
	public AndroidDriver<MobileElement> driver = null;
	
	
	
	protected abstract AndroidDriver<MobileElement> createDriver() throws Exception;
	
	public AndroidDriver<MobileElement> getDriver() throws Exception{
		System.out.println("Inside DriverManager.getDriver()");
		System.out.println("Is AndroidDriver null ? --> " + driver == null);
		
		if(driver == null){
			MyLogger.log.info("DriverManager.getDriver: Creating a driver");
			driver = createDriver(); 
			System.out.println(" heyyya2: " + driver == null);
		}
		System.out.println("Driver is not null-->" + driver == null);
		return driver;
	}
	
	public String toString(){
		return "DriverManager class toString method";
	}
}
