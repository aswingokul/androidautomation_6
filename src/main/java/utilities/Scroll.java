/**
 * 
 */
package utilities;

import org.openqa.selenium.Dimension;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

/**
 * @author aswin
 *
 */
public class Scroll {
	
	public static void down(AndroidDriver<MobileElement> driver){
		Dimension viewPort = driver.manage().window().getSize();
		System.out.println("viewPort Height: " + viewPort.getHeight());
		System.out.println("viewPort Width: " + viewPort.getWidth());
		
		int startX = viewPort.getWidth() / 2;
		int startY = viewPort.getHeight() / 2;
		
		int endX = startX;
		int endY = (int) (viewPort.height * 100 / viewPort.height);
		System.out.println("endX: " + endX);
		System.out.println("endY: " + endY);
		TouchAction touch =new TouchAction(driver);
		PointOption<ElementOption> point = new PointOption<ElementOption>();
//		Map<String, Object> map = point.build();
		point = PointOption.point(0, 0);
		PointOption endPoint = new PointOption();
		endPoint = PointOption.point(endX, endY);
		touch.longPress(point).waitAction().moveTo(endPoint).release().perform();
	}

}
