/**
 * 
 */
package utilities;

import java.util.Random;

/**
 * @author aswin
 *
 */
public class EmailGenerator {
	
	public static String generate(){
		String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String lower = upper.toLowerCase();
		String nums = "1234567890";
		String space = upper + lower + nums;
		StringBuilder randomString = new StringBuilder();
		Random rnd = new Random();
		while(randomString.length() < 10){
			int index = (int) (rnd.nextFloat() * 64);
			randomString.append(space.charAt(index));
		}
		randomString.append("@saavn.com");
		System.out.println("random email: " + randomString.toString());
		return randomString.toString();
	}

}
