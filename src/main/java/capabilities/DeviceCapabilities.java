/**
 * 
 */
package capabilities;

import java.io.File;
import java.util.HashMap;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.gson.Gson;


import enums.DriverType;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import managers.DriverConnection;
import managers.DriverManager;

/**
 * @author aswin
 *
 */
public class DeviceCapabilities extends DriverManager {
	private AndroidDriver<MobileElement> driver = null;
	/*
	 * (non-Javadoc)
	 * @see managers.DriverManager#createDriver()
	 * 
	 * Provides a stage for creating a driver based upon the given capabilities
	 * This class provides an implementation of the DriverManager's createDriver method
	 */

	@Override
	protected AndroidDriver<MobileElement> createDriver() throws Exception {
		System.out.println("mofu1 ");
		DesiredCapabilities capabilities = new DesiredCapabilities();
//		DeviceInfo devInfo = new DeviceInfoImpl(DeviceType.ANDROID);
//		Device myDevice = devInfo.getFirstDevice();
		File app = new File("src/test/resources/app/saavn.apk");
		System.out.println("mofu2 ");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Samsung Galaxy Note 2");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.4.2");
		capabilities.setCapability(MobileCapabilityType.UDID, "4d0012b8460eb04b");
		capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		
		System.out.println("DeviceCapabilities - Creating a connection");
		driver =  DriverConnection.getDriver(DriverType.DEVICE, capabilities);
		System.out.println("Driver created in createDriver(): " + driver.toString());
		
		System.out.println(" heyyya1 " +driver.toString());
		return driver;
		
		
	}
	
	public String toString(){
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put("Device Name", "Google Pixel XL");
		map.put("Platform Name", "Android");
		map.put("Platform Version", "7.1");
		map.put("Unique Device ID", "HT72F0201033");
		map.put("App", "src/test/resources/app/saavn.apk");
		
		return map.toString();
		
	}
	
	public AndroidDriver<MobileElement> getDriver() throws Exception{
		return createDriver();
	}

}
