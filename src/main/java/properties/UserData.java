/**
 * 
 */
package properties;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

/**
 * @author aswin
 *
 */
public class UserData {
	private String username = null;
	private String password = null;
	private String userCookie = null;
	
	
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getUserCookie() {
		return userCookie;
	}

	public void ROW_user() throws FileNotFoundException{
		JsonReader reader = new JsonReader(new FileReader("src/main/java/properties/ROW_user.json"));
		JsonElement elem = new Gson().fromJson(reader, JsonElement.class);
		JsonObject userData = elem.getAsJsonObject(); 
		getFields(userData);
		
	}
	
	public void SAARC_user() throws FileNotFoundException{
		JsonReader reader = new JsonReader(new FileReader("src/main/java/properties/SAARC_user.json"));
		JsonElement elem = new Gson().fromJson(reader, JsonElement.class);
		JsonObject userData = elem.getAsJsonObject(); 
		getFields(userData);
	}
	
	public void getFields(JsonObject userData){
		username = userData.get("username").getAsString();
		password = userData.get("password").getAsString();
	}
}
