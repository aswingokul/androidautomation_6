/**
 * 
 */
package properties;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;


/**
 * @author aswin
 *
 */
@Sources({"classpath:${env}.properties"})
public interface Environment extends Config{
	String getAppPath();
	String getBaseUrl();
	String getLanguageCookie();
}
