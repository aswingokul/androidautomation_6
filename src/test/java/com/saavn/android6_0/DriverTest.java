/**
 * 
 */
package com.saavn.android6_0;

import org.testng.annotations.BeforeTest;

import enums.DriverType;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import managers.DriverManager;
import managers.DriverManagerFactory;

/**
 * @author aswin
 *
 */
public class DriverTest {
	private static AndroidDriver<MobileElement> driver = null;
	
	private void connectDriver(){
		DriverManager driverManager = DriverManagerFactory.getManager(DriverType.DEVICE);
		try {
			System.out.println("Inside DriverTest.connectDriver()");
			driver = driverManager.getDriver();
			System.out.println("Driver is connected - is it null? -->" + driver == null);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static AndroidDriver<MobileElement> getDriver(){
		if(driver == null){
			new DriverTest().connectDriver();
		}
		return driver;
	}

}
