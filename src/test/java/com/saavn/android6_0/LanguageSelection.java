/**
 * 
 */
package com.saavn.android6_0;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import utilities.Scroll;

/**
 * @author aswin
 *
 */
public class LanguageSelection {
	
	AndroidDriver<MobileElement> driver = DriverTest.getDriver();
	
	
	public void selectAllLang(){
		if(driver != null){
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("done")));
			
			driver.findElementById("tamilimageViewOn").click();
			driver.findElementById("teluguimageViewOn").click();
			driver.findElementById("punjabiimageViewOn").click();
			
			int scrollCount = 0;
			
			while(scrollCount++ < 2){
				Scroll.down(driver);
			}
			
			List<MobileElement> otherElems = driver.findElementsById("langlistitem");
			
			for(MobileElement elem : otherElems){
				elem.click();
			}
			
			driver.findElementById("done");
		}else{
			System.out.println("AndroidDriver is null");
		}
	}
	
	
	public void selectDefaultLanguages(){
		if(driver != null){
			driver.findElementById("done").click();
		}
	}
	
	@Test
	public void languageTests(){
		System.out.println("----------Language Selection Test-----------");
//		selectAllLang();
		selectDefaultLanguages();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("----------End of Language Selection Test-----------");
	}
	
	
	

}
