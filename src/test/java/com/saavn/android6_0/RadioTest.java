/**
 * 
 */
package com.saavn.android6_0;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import pageObjects.QueuePlayer;
import pageObjects.RadioPlayer;

/**
 * @author aswin
 *
 */
public class RadioTest {

	AndroidDriver<MobileElement> driver = DriverTest.getDriver();
	
	@Test
	public void testRadioPage(){
		System.out.println("---------Radio Test---------");
		openRadioHome();
		verifyRadio();
		System.out.println("---------End of Radio Test---------");
	}

	public void openRadioHome() {
		if (driver != null) {
			driver.findElementById("browseIcon").click();

			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			driver.findElementById("radioContainer").click();

			WebDriverWait wait = new WebDriverWait(driver, 20, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("radioPic")));

		} else {
			System.out.println("Driver is null");
		}
	}

	public void verifyRadio() {
		if (driver != null) {
			List<MobileElement> radioTiles = driver.findElementsById("radioTileLayout");
			int stnCount = 0;
			for (MobileElement station : radioTiles) {
//				MobileElement metadata = station.findElementById("metadata");
//				String stationTitle = metadata.findElementById("title").getText();

				station.click();

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

//				driver.findElementById("player_title").click();
				
				RadioPlayer radio = new RadioPlayer(driver);
				System.out.println("Let's go on skipping the songs...");
				
				WebDriverWait wait  = new WebDriverWait(driver, 10);
				
				int skipcount = 0;
				
				while (skipcount < 5) {
					System.out.println("Inside loop...\n");
					String currSong = radio.getCurrentSong();
					radio.playNext();
					System.out.println("Skipped song - " + ++skipcount);
					wait.until(ExpectedConditions.not(ExpectedConditions.
							attributeContains(By.id("com.saavn.android:id/curr_song_title"), "text", currSong)));
					System.out.println("I waited till the song changed.... ");
//					radio.dislikeSong();					
//					currSong = radio.getCurrentSong();
//					wait.until(ExpectedConditions.not(ExpectedConditions.
//							attributeContains(By.id("com.saavn.android:id/curr_song_title"), "text", currSong)));
				}
				driver.navigate().back();
				stnCount++;
				if(stnCount > 3) break;
				
			}
		}

	}

	void skipRadioSong() {
		// Get the name of the current song
		String currSong = driver.findElementById("curr_song_title").getText();

		// Get the radio dislike button
		driver.findElement(By.id("radiodislikebutton")).click();
		
		

		// Wait till the curr song changes
		WebDriverWait wait = new WebDriverWait(driver, 5000);
		wait.until(ExpectedConditions
				.not(ExpectedConditions.textToBePresentInElementValue(By.id("curr_song_title"), currSong)));

		
	}

}
