package com.saavn.android6_0;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import capabilities.DeviceCapabilities;
import enums.DriverType;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import managers.DriverManager;
import managers.DriverManagerFactory;
import managers.ServerManager;
import utilities.EmailGenerator;

/**
 * Unit test for simple App.
 */
public class LoginPageTest

{
	AndroidDriver<MobileElement> driver = null;

	String email = null;
	String password = null;
	boolean signup = false;

	@BeforeTest
	public void createDriver() {
		driver = DriverTest.getDriver();

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@AfterTest
	public void quit() {
		if (driver != null)
			driver.quit();
	}

	@Test
	public void testLoginPage() {
		System.out.println("-------Test Login Page-------");
		loginOrSignUp("login");
//		loginOrSignUp("signup");
		System.out.println("-------End of LoginTest------");
	}

	void loginOrSignUp(String op) {
		if(op.equals("login")){
			email = "tanu@saavn.com";
			password = "tanu123";

			testLogin();
		}else{		
			email = EmailGenerator.generate();
			password = "saavn123";
			this.signup = true;
			testSignUp();
			
		}
	}

	public void testSignUp() {
		System.out.println("-------Sign Up test-------");
		String email = EmailGenerator.generate();
		System.out.println("Email for signup: " + email);

		if (driver != null) {
			MobileElement topRightButton = driver.findElement(By.id("topRightButton"));

			if (topRightButton.getText().equals("Log In")) {
				populateFields();
			}

		}else{
			System.out.println("AndroidDriver is null");
		}
	}

	public void testLogin() {
		System.out.println("------Login Test------");
		if (driver != null) {
			MobileElement topRightButton = driver.findElement(By.id("topRightButton"));

			if (topRightButton.getText().equals("Log In")) {
				topRightButton.click();
			}

			populateFields();

		}
		else{
			System.out.println("AndroidDriver is null");
		}
	}

	void populateFields() {
		if (driver != null) {
			MobileElement loginWithMail = driver.findElement(By.id("email"));
			loginWithMail.click();
			
			
			if(this.signup){
				WebDriverWait wait = new WebDriverWait(driver, 10);
				System.out.println("Waiting until google autofill is visible......");
//				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("credential_picker_layout")));
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				MobileElement googleAutoFill = driver.findElementById("com.google.android.gms:id/credential_picker_layout");
				googleAutoFill.findElementById("cancel").click();
				System.out.println("Dismissed google autofillXXXXXXX");
			}
			

			// Get the email
			MobileElement emailField = driver.findElement(By.id("userEmail"));

			MobileElement password = driver.findElement(By.id("userPassword"));
			
			emailField.sendKeys(this.email);
			password.sendKeys(this.password);
			
			MobileElement signUpLoginBtn = driver.findElement(By.id("signUpLogInButton"));
			signUpLoginBtn.click();
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			System.out.println("AndroidDriver is null");
		}
	}
}
